# -*- coding: utf-8 -*-'

"""
Minimalist redis cli tool for DD Peng.Xu & Chen.Zhai

@author: Bingxing.Kang
"""

import sys
import argparse
import redis
import traceback
from termcolor import *


def die(message, code=1):
    print >>sys.stderr, message
    sys.exit(code)


class Dead(Exception):
    def __init__(self, msg):
        self._msg = msg

    def __str__(self):
        return self._msg


class Go(object):
    def __init__(self):
        super(Go, self).__init__()

    def run(self, cmdargs):
        parser = argparse.ArgumentParser(
            prog="{} ".format(sys.argv[0].split(os.path.sep)[-1]),
            description=self.__doc__
        )

        parser.add_argument(
            '-H', '--host', default='datadeck-redis-6379-v3-a0f1eb47117f0fe0.elb.ap-northeast-1.amazonaws.com',
            help="password, any question? Ask Peng.Xu")

        parser.add_argument(
            '-P', '--port', default=6379,
            help="port, any question? Ask Peng.Xu")

        parser.add_argument(
            '-p', '--password', default=None,
            help="password, any question? Ask Peng.Xu")

        if not cmdargs:
            sys.exit(parser.print_help())

        args = parser.parse_args(args=cmdargs)

        if not args.password:
            die('All we need is a password ...')


        try:
            client = connect(args.host, args.port, args.password)
        except Dead as d:
            print d
            print "Sorry, failed to connect..." + "\n" + \
                  "Host: " + str(args.host) + "\n" + \
                  "Port: " + str(args.port) + "\n" + \
                  "Password: " + "*"*3 + "..." + str(args.password[-3:])
            die("")
        else:
            print "success to connect to ..."+ "\n" + \
                  "Host: " + str(args.host) + "\n" + \
                  "Port: " + str(args.port) + "\n" + \
                  "Password: " + "*"*3 + "..." + str(args.password[-3:])
            while 1:
                print "+ " + "-"*30 + " +"
                action = raw_input(colored("Which action: c, r, u, d: \n", "yellow"))
                if action in ['c']:
                    key, value, px = raw_input(colored("Key\n:", "red")), \
                                     raw_input(colored("Value\n:", "red")), \
                                     raw_input(colored("Expiration(ms), 0 for never\n:", "red"))
                    try:
                        res = client.set(key, value, px=int(px) if int(px) else None)
                    except Exception:
                        res = Dead(traceback.format_exc())
                    print colored("Result\n:{}".format(res), "yellow")

                if action in ['r']:
                    key = raw_input(colored("Key\n:", "red"))
                    try:
                        if "*" in key:
                            res = "\n".join(client.keys(pattern=key))
                        else:
                            res = str(client.get(key))
                    except Exception:
                        res = Dead(traceback.format_exc())
                    print colored("Result\n:{}".format(res), "yellow")

                if action in ['u']:
                    key, value = raw_input(colored("Key\n:", "red")), raw_input(colored("Value\n:", "red"))
                    try:
                        res = client.set(key, value)
                    except Exception:
                        res = Dead(traceback.format_exc())
                    print colored("Result\n:{}".format(res), "yellow")

                if action in ['d']:
                    raw_input(colored("Key\n:", "red"))
                    try:
                        if "*" in key:
                            res = client.delete(*client.keys(pattern=key))
                        else:
                            res = str(client.delete(key))
                    except Exception:
                        res = Dead(traceback.format_exc())
                        print colored("Result\n:{}".format(res), "yellow")


def connect(host, port, pw):
    try:
        client = redis.StrictRedis(
            host=host,
            port=port,
            password=pw,  # 'Ptminddatadeck2018!'
            ssl=True
        )
        # excute only key `_bingxing` exists, just checking password
        client.set('_bingxing', 'ok', xx=True)
        return client
    except Exception:
        raise Dead(traceback.format_exc())


def main():
    args = sys.argv[1:]
    if not len(args) > 0:
        die('Where\'s your Arguments? use `-h` for help')

    Go().run(args)


if __name__ == '__main__':
    main()
